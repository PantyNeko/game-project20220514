using System;
using System.Collections.Generic;

namespace QFramework
{
    public class ResPool<T> where T:UnityEngine.Object
    {
        private Dictionary<string, T> mResDic = new Dictionary<string, T>();

        public void Get(string key, Action<T> callback)
        {
            if (mResDic.TryGetValue(key, out T data))
            {
                callback(data);
                return;
            }
            mResDic.Add(key, null);
            ResHelper.AsyncLoad<T>(key, o =>
            {
                callback(o);
                mResDic[key] = o;
            });
        }
        /// <summary>
        /// ������Դ
        /// </summary>
        public void Clear() => mResDic.Clear();
    }
}