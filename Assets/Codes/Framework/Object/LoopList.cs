﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework
{
    [System.Serializable]
    public class LoopList<T> : IEnumerable<T>
    {
        [SerializeField] private List<T> mItems;
        [SerializeField] private int vernier = 0;
        public T Current => mItems[vernier];
        public LoopList(int capacity)
        {
            mItems = new List<T>(capacity);
        }
        public LoopList(List<T> items)
        {
            mItems = items;
        }
        public LoopList() : this(2)
        {

        }
        // 索引器
        public T this[int index]
        {
            get
            {
                return mItems[index];
            }
            set
            {
                mItems[index] = value;
            }
        }

        public void Add(T e)
        {
            mItems.Add(e);
        }
        public void RemoveAt(int index)
        {
            if (vernier > index) LoopNeg();
            mItems.RemoveAt(index);
        }
        public void LoopNeg()
        {
            vernier = (vernier + mItems.Count - 1) % mItems.Count;
        }
        public void LoopPos()
        {
            vernier = (vernier + 1) % mItems.Count;
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            for (int i = 0; i < mItems.Count; i++)
            {
                yield return mItems[i];
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < mItems.Count; i++)
            {
                yield return mItems[i];
            }
        }
    }
}