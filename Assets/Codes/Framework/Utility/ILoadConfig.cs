﻿namespace QFramework
{
    public interface IJsonConfig : IUtility
    {
        bool Load<T>(string fileName, out T data) where T : class;
    }
    public interface ICsvConfig : IUtility
    {
        bool Load<T>(string fileName, out T data) where T : class;
    }
    public class LoadConfig : IJsonConfig, ICsvConfig
    {
        bool IJsonConfig.Load<T>(string fileName, out T data)
        {
            data = SerializeHelper.LoadJson<T>(CommonData.ConfigPath + fileName + ".json");
            return data != null;
        }
        bool ICsvConfig.Load<T>(string fileName, out T data)
        {
            data = default;
            return true;
        }
    }
}