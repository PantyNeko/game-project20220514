﻿using System;
using UnityEngine;

namespace QFramework
{
    public class CustomEquip : MonoBehaviour
    {
        [SerializeField]
        protected LoopList<EquipInfo> mEquips;
        public event Action<EquipInfo> EquipConfigEvent;
        protected void SendConfigEvent()
        {
            EquipConfigEvent?.Invoke(mEquips.Current);
        }
        public EquipInfo Current => mEquips.Current;
        public void Add(EquipInfo equip) => mEquips.Add(equip);
        public void Remove(int index) => mEquips.RemoveAt(index);
        public void Change(bool isNeg)
        {
            if (isNeg) mEquips.LoopNeg();
            else mEquips.LoopPos();
        }
        public EquipInfo this[int index]
        {
            get => mEquips[index];
            set => mEquips[index] = value;
        }
    }
}