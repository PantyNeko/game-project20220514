﻿namespace QFramework
{
    // 武器脚本 无论是玩家还是敌人 都具备拾取和更换武器的能力 CustomEquip
    public abstract class CustomWeapon : CustomEquip
    {
        // 需要在子类定义不同能力槽的逻辑 例如 按A 执行当前武器 xx逻辑
        public void Use(byte abilityIndex)
        {
            SendConfigEvent();
            Use(mEquips.Current, abilityIndex);
        }
        protected abstract void Use(EquipInfo info, byte abilityIndex);
    }
}