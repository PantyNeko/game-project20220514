﻿using UnityEngine;

namespace PlatformShoot
{
    [CreateAssetMenu(fileName = "New TestAI Data", menuName = "Data/TestAIData")]
    public class TestAIData : ScriptableObject
    {
        public float GroundPatrolSpeed = 5f;
        public float GroundPursuitSpeed = 8f;
        public float GroundRayLength = 1f;
        public float ObserveWaitTime = 1f;
        public float JumpForce = 12;
        public float InAirMoveSpeed = 7;

        public Vector2 GroundCheckBoxSize;

        public Vector2 ObserveCheckBoxSize;        
        public Vector3 ObserveCheckOffset;

        public Vector2 PursuitCheckBoxSize;
        public Vector2 PursuitCheckOffset;

        public Vector2 TriggerJumpRange;

        public LayerMask GroundLayer;
        public LayerMask PlayerLayer;
    }
}