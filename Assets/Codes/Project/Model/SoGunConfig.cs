﻿using UnityEngine;
using QFramework;

namespace PlatformShoot
{
    [CreateAssetMenu(fileName = "SO_GunConfig", menuName = "Data/SO/SO_GunConfig")]
    public class SoGunConfig : SO_EquipConfig<GunConfig> { }
}