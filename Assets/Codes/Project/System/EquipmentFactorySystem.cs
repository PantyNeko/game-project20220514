﻿using QFramework;

namespace PlatformShoot
{
    public class EquipmentFactorySystem : AbstractEquipmentFactorySystem
    {
        protected override EquipInfo CreateRules(E_EquipType type, EquipConfig equip)
        {
            switch (type)
            {
                case E_EquipType.Gun:
                    return (equip as IEquipConfig<GunInfo>).NewWeapon;
                case E_EquipType.Armor:
                    // return (equip as IEquipConfig<ArmorInfo>).NewWeapon;
                case E_EquipType.Helmet:
                    // return (equip as IEquipConfig<HelmetInfo>).NewWeapon;
                    break;
            }
            return null;
        }
        protected override void OnInit()
        {
            base.OnInit();
            // CreateFactory<GunConfig>(E_EquipType.Gun);
            CreateFactoryBySO("SO_GunConfig", E_EquipType.Gun);
            // CreateFactory<ArmorConfig>(E_EquipType.Armor);
        }
    }
}