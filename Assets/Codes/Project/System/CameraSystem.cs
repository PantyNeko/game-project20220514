using UnityEngine;
using QFramework;

namespace PlatformShoot
{
    public interface ICameraSystem : ISystem
    {
        void SetTarget(ICamTarget target);
    }
    public interface ICamTarget
    {
        Vector2 Pos { get; }
    }
    public class AdvancedCameraSystem : AbstractSystem, ICameraSystem
    {
        private ICamTarget mTarget;

        private Vector3 mTargetPos;

        private float mSmoothTime = 2;

        /// <summary>
        /// 摄像机限制范围
        /// </summary>
        private float minX = -100f, minY = -100f, maxX = 100f, maxY = 100f;

        protected override void OnInit()
        {
            PublicMono.Instance.OnFixedUpdate += Update;
            mTargetPos.z = -10;
        }

        private void Update()
        {
            if (mTarget.Equals(null)) return;
            // 获取摄像机
            mTargetPos.x = Mathf.Clamp(mTarget.Pos.x, minX, maxX);
            mTargetPos.y = Mathf.Clamp(mTarget.Pos.y, minY, maxY);

            var cam = Camera.main.transform;

            if ((cam.position - mTargetPos).sqrMagnitude < 0.01f) return;
            cam.localPosition = Vector3.Lerp(cam.position, mTargetPos, mSmoothTime * Time.deltaTime);
        }
        void ICameraSystem.SetTarget(ICamTarget target)
        {
            mTarget = target;
        }
    }
}