﻿using QFramework;
using UnityEngine;

namespace PlatformShoot
{
    // 整合当前项目接入的不同武器系统
    public interface IWeaponSystem : ISystem
    {
        void Use(EquipInfo info, byte abilityIndex);
    }

    public class WeaponSystem : AbstractSystem, IWeaponSystem
    {
        protected override void OnInit()
        {

        }

        void IWeaponSystem.Use(EquipInfo info, byte abilityIndex)
        {
            switch (info.Type)
            {
                case E_EquipType.Gun:
                    switch (abilityIndex)
                    {
                        case 0: // 射击
                            this.GetSystem<IGunSystem>().Shoot(info as GunInfo);
                            break;
                        case 1: // 装填
                            this.GetSystem<IGunSystem>().Reload(info as GunInfo);
                            break;
                    }
                    break;
                case E_EquipType.Sword:
                    switch (abilityIndex)
                    {
                        case 0: // 劈砍
                            this.GetSystem<ISwordSystem>().Chop(info as SwordInfo);
                            break;
                        case 1: // 格挡
                            this.GetSystem<ISwordSystem>().Parry(info as SwordInfo);
                            break;
                    }
                    break;
                case E_EquipType.Shield:
                    switch (abilityIndex)
                    {
                        case 0: // 防御
                            Debug.Log("防御");
                            break;
                        case 1: // 格挡
                            Debug.Log("格挡");
                            break;
                    }
                    break;
            }
        }
    }
}