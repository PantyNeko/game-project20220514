﻿using QFramework;
using UnityEngine;

namespace PlatformShoot
{
    public class SwordInfo : EquipInfo
    {
        public float aggressivity;
        public SwordInfo()
        {
            Type = E_EquipType.Sword;
        }
    }
    public class SwordConfig : EquipConfig, IEquipConfig<SwordInfo>
    {
        SwordInfo IEquipConfig<SwordInfo>.NewWeapon => new SwordInfo();
    }
    public interface ISwordSystem : ISystem
    {
        void Chop(SwordInfo sword);
        void Parry(SwordInfo sword);
    }
    public class SwordSystem : AbstractSystem, ISwordSystem
    {
        protected override void OnInit()
        {

        }

        void ISwordSystem.Chop(SwordInfo sword)
        {
            Debug.Log(sword.aggressivity);
        }

        void ISwordSystem.Parry(SwordInfo sword)
        {
            Debug.Log(sword.Name);
        }
    }
}