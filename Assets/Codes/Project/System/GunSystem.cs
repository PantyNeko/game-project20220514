﻿using QFramework;

namespace PlatformShoot
{
    [System.Serializable]
    public class GunConfig : EquipConfig, IEquipConfig<GunInfo>
    {
        public int capacity;
        public float frequency;
        public float reloadTime;
        public int consumptionQuantity;

        public GunConfig(string name, int capacity, float frequency, float reloadTime, int consumptionQuantity)
        {
            this.Name = name;
            this.capacity = capacity;
            this.frequency = frequency;
            this.reloadTime = reloadTime;
            this.consumptionQuantity = consumptionQuantity;
        }
        GunInfo IEquipConfig<GunInfo>.NewWeapon => new GunInfo(Name, capacity, frequency, reloadTime, consumptionQuantity);
    }
    public interface IGunSystem : ISystem
    {
        void Shoot(GunInfo gun);
        void Reload(GunInfo gun);
    }
    public class GunSystem : AbstractSystem, IGunSystem
    {
        protected override void OnInit()
        {

        }
        void IGunSystem.Reload(GunInfo gun)
        {
            Reload(gun);
        }
        void IGunSystem.Shoot(GunInfo gun)
        {
            switch (gun.State)
            {
                case GunInfo.E_State.Idle:
                    if (gun.TryShoot())
                    {
                        // 播放攻击音效
                        this.GetSystem<IAudioMgrSystem>().PlaySound("竖琴");
                        // 异步加载子弹预制体 设置子弹生成位置 设置子弹的方向
                        this.GetSystem<IObjectPoolSystem>().Get("Item/Bullet", o =>
                        {
                            o.transform.localPosition = gun.shootPoint;
                            o.GetComponent<Bullet>().InitDir(gun.shootDir);
                        });
                    }
                    break;
                case GunInfo.E_State.CoolDown:
                    this.GetSystem<ITimerSystem>().AddTimer(gun.frequency, () =>
                    {
                        gun.State = GunInfo.E_State.Idle;
                    });
                    gun.State = GunInfo.E_State.Wait;
                    break;
                case GunInfo.E_State.Reload:
                    Reload(gun);
                    break;
            }
        }

        private void Reload(GunInfo gun)
        {
            this.GetSystem<ITimerSystem>().AddTimer(gun.reloadTime, () =>
            {
                gun.State = GunInfo.E_State.Idle;
                gun.Reload();
            });
            gun.State = GunInfo.E_State.Wait;
        }
    }
}
