using QFramework;

namespace PlatformShoot
{
    public class PlayerWeapon : CustomWeapon, IController
    {
        protected override void Use(EquipInfo info, byte abilityIndex)
        {
            this.GetSystem<IWeaponSystem>().Use(info, abilityIndex);
        }
        private void Start()
        {
            mEquips = new LoopList<EquipInfo>(this.GetSystem<IEquipmentFactorySystem>().CreateAllEquip());
        }
        IArchitecture IBelongToArchitecture.GetArchitecture() => PlatformShootGame.Interface;
    }
}