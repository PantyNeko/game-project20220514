using UnityEngine;
using QFramework;
using System;

namespace PlatformShoot
{
    public class Player : PlatformShootGameController, ICamTarget
    {
        private Rigidbody2D mRig;
        private CustomWeapon mWeapon;
        private PlayerInputHandle inputHandle;
        private BoxCollider2D mBoxColl;
        private LayerMask mGroundLayer;

        private float mAccDelta = 120f;
        private float mDecDelta = 180f;
        private float mGroundMoveSpeed = 5f;
        private float mJumpForce = 13f;


        [SerializeField] private int mJumpCount;
        [SerializeField] private int maxJumpCount = 2;
        private int mFaceDir = 1;
        private bool isJumping;
        private Vector2 mCurSpeed;

        private bool mGround;

        private IAudioMgrSystem audioMgr;


        Vector2 ICamTarget.Pos => transform.position;

        private void Awake()
        {
            this.SendCommand<InitGameCommand>();
            mWeapon = GetComponentInChildren<CustomWeapon>();
            mWeapon.EquipConfigEvent += OnUseWeapon;

            inputHandle = GetComponent<PlayerInputHandle>();
        }
        private void OnDestroy()
        {
            mWeapon.EquipConfigEvent -= OnUseWeapon;
        }
        private void OnUseWeapon(EquipInfo weapon)
        {
            switch (weapon.Type)
            {
                case E_EquipType.Gun:
                    var gun = weapon as GunInfo;
                    gun.shootDir = Vector2.right * mFaceDir;
                    gun.shootPoint = transform.position;
                    break;
                case E_EquipType.Sword:
                    var sword = weapon as SwordInfo;
                    sword.aggressivity = UnityEngine.Random.Range(0, 10);
                    break;
                case E_EquipType.Shield:
                    break;
            }
        }

        private void Start()
        {
            mRig = GetComponent<Rigidbody2D>();
            mBoxColl = GetComponentInChildren<BoxCollider2D>();
            mGroundLayer = LayerMask.GetMask("Ground");

            this.GetSystem<ICameraSystem>().SetTarget(this);

            audioMgr = this.GetSystem<IAudioMgrSystem>();

            audioMgr.PlayBgm("��ɫ֮��");
        }
        private void Update()
        {
            mGround = Physics2D.OverlapBox(transform.position + mBoxColl.size.y * Vector3.down * 0.5f, new Vector2(mBoxColl.size.x * 0.8f, 0.1f), 0, mGroundLayer);

            // ���
            if (inputHandle.AttackInput)
            {
                // mWeapon.Shoot(mFaceDir);
                mWeapon.Use(0);
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var coll = Physics2D.OverlapBox(transform.position, Vector2.one, 0, ~LayerMask.GetMask("Player"));
                if (coll)
                {
                    var item = coll.GetComponent<IInteractiveItem<EquipInfo>>();
                    if (item != null) mWeapon.Add(item.Trigger());
                }
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                mWeapon.Change(false);
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                mWeapon.Use(1);
            }
            if (mGround)
            {
                mJumpCount = maxJumpCount;
            }

            if (mGround && isJumping)
            {
                audioMgr.PlaySound("���2");
                isJumping = false;
            }
            mCurSpeed = mRig.velocity;

            if (inputHandle.InputX != 0)
            {
                if (inputHandle.InputX != mFaceDir) Flip();
                mCurSpeed.x = Mathf.Clamp(mCurSpeed.x + inputHandle.InputX * mAccDelta * Time.deltaTime, -mGroundMoveSpeed, mGroundMoveSpeed);
            }
            else
            {
                mCurSpeed.x = Mathf.MoveTowards(mCurSpeed.x, 0, mDecDelta * Time.deltaTime);
            }
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            if (mBoxColl == null) return;
            Gizmos.DrawWireCube(transform.position + mBoxColl.size.y * Vector3.down * 0.5f, new Vector2(mBoxColl.size.x * 0.9f, 0.1f));
        }
        private void FixedUpdate()
        {
            if (mJumpCount == 0)
            {
                inputHandle.JumpInput = false;
            }
            else if (inputHandle.JumpInput)
            {
                mJumpCount--;
                isJumping = true;
                inputHandle.JumpInput = false;
                mCurSpeed.y = mJumpForce;
                audioMgr.PlaySound("��Ծ");
            }

            mRig.velocity = mCurSpeed;
        }

        private void Flip()
        {
            mFaceDir = -mFaceDir;
            transform.Rotate(0, 180, 0);
        }
        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.CompareTag("Interactive"))
            {
                coll.GetComponent<IInteractiveItem>()?.Trigger();
            }
        }
    }
}