using QFramework;

namespace PlatformShoot
{
    public interface IInteractiveItem
    {
        void Trigger();
    }
    public interface IInteractiveItem<T>
    {
        T Trigger();
    }

    public class PitfallObj : PlatformShootGameController, IInteractiveItem
    {
        void IInteractiveItem.Trigger()
        {
            this.SendCommand(new NextLevelCommand("GamePassScene"));
        }
    }
}