using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformShoot
{
    /// <summary>
    /// 支持多人的单向平台组件
    /// 需要挂载 PlatformEffector2D 组件 Collider2D 组件
    /// 为了让程序不会写死 外部需要手动检测脚下是否是单向平台 调用 DisableCollision 方法即可
    /// </summary>
    public class TwoWayPlatform : MonoBehaviour
    {
        private HashSet<Collider2D> targets;
        private Collider2D mPlatform;
        [SerializeField] private float recoveryTime = 0.3f;

        private void Start()
        {
            targets = new HashSet<Collider2D>();
            mPlatform = GetComponent<Collider2D>();
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (targets.Add(other.collider))
            {
                // Debug.Log($"添加新对象{other.gameObject.name}");
            }
        }
        private void OnCollisionExit2D(Collision2D other)
        {
            if (targets.Remove(other.collider))
            {
                // Debug.Log($"移除新对象{other.gameObject.name}");
            }
        }
        public void DisableCollision(Collider2D other)
        {
            if (targets.Contains(other))
            {
                StartCoroutine(_DisableCollision(other));
            }
        }
        public void DisableCollision(GameObject o)
        {
            DisableCollision(o.GetComponent<Collider2D>());
        }
        private IEnumerator _DisableCollision(Collider2D other)
        {
            Physics2D.IgnoreCollision(mPlatform, other, true);
            yield return new WaitForSeconds(recoveryTime);
            Physics2D.IgnoreCollision(mPlatform, other, false);
        }
    }
}