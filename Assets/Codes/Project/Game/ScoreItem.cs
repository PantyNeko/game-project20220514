using UnityEngine;
using QFramework;

namespace PlatformShoot
{
    public class ScoreItem : PlatformShootGameController, IInteractiveItem
    {
        void IInteractiveItem.Trigger()
        {
            GameObject.Destroy(gameObject);
            this.GetModel<IGameModel>().Score.Value++;
            // 拾取道具音效
            this.GetSystem<IAudioMgrSystem>().PlaySound("拾取金币");
        }
    }
}