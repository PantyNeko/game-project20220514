using QFramework;

namespace PlatformShoot
{
    public class BagItem : PlatformShootGameController, IInteractiveItem<EquipInfo>
    {
        public int nameId;
        public E_EquipType type;

        private void Start()
        {
            //this.GetModel<IWeaponConfigModel>().GetConfig<GunConfig>(E_WeaponType.Gun, 0, out var config);
            //Debug.Log(config.Name);
            //this.GetModel<IWeaponConfigModel>().Create<GunInfo>(E_WeaponType.Gun, 1, out var info);
            //Debug.Log(info.Name);
        }

        EquipInfo IInteractiveItem<EquipInfo>.Trigger()
        {
            if (this.GetSystem<IEquipmentFactorySystem>().CreateEquip(type, nameId, out EquipInfo equip))
            {
                // 如何 让 背包对象 跟 武器进行交互
                Destroy(gameObject);
            }
            return equip;
        }
    }
}