﻿using UnityEngine;
using QFramework;

namespace PlatformShoot
{
    // 指代每一把枪
    // [System.Serializable]
    public class GunInfo : EquipInfo
    {
        public enum E_State : byte
        {
            Idle,
            CoolDown,
            Reload,
            Wait
        }

        public Vector2 shootDir;
        public Vector2 shootPoint;

        public int remainingCount;
        public int capacity;
        public int consumptionQuantity;
        public float frequency;
        public float reloadTime;

        public E_State State = E_State.Idle;

        public GunInfo(string name, int capacity, float frequency, float reloadTime, int consumptionQuantity)
        {
            this.Name = name;
            this.capacity = capacity;
            this.frequency = frequency;
            this.reloadTime = reloadTime;
            this.consumptionQuantity = consumptionQuantity;
            remainingCount = capacity;
            Type = E_EquipType.Gun;
        }

        public GunInfo(GunInfo info) :
            this(info.Name, info.capacity, info.frequency, info.reloadTime, info.consumptionQuantity)
        { }

        public void Reload()
        {
            remainingCount = capacity;
        }
        public bool TryShoot()
        {
            if (remainingCount >= consumptionQuantity)
            {
                remainingCount -= consumptionQuantity;
                State = E_State.CoolDown;
                return true;
            }
            else
            {
                State = E_State.Reload;
                return false;
            }
        }
    }
}