using UnityEngine;
using UnityEngine.UI;
using QFramework;

namespace PlatformShoot
{
    public class MainPanel : PlatformShootUIController
    {
        private Text mScoreTex;

        private void Start()
        {
            mScoreTex = GetControl<Text>("ScoreTex");

            this.GetModel<IGameModel>().Score.RegisterWithInitValue(OnScoreChanged)
                .UnRegisterWhenGameObjectDestroyed(gameObject);
        }
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "SettingBtn":
                    this.GetSystem<IUGUISystem>().OpenPanel<SettingPanel>(E_UILayer.Top, panel => Debug.Log($"��{panel}���"));
                    break;
            }
        }
        private void OnScoreChanged(int score)
        {
            mScoreTex.text = score.ToString();
        }

        public void UpdateScoreTex(int score)
        {
            int temp = int.Parse(mScoreTex.text);
            mScoreTex.text = (temp + score).ToString();
        }
    }
}