using UnityEngine;
using QFramework;

namespace PlatformShoot
{
    public class JoystickPanel : PlatformShootUIController, ICanSendEvent
    {
        private DirInputEvent mDirInput;

        protected override void Awake()
        {
            base.Awake();

            transform.Find("LeftStickArea")
                .GetComponent<StickDrag>()
                .OnDirChange += ((x, y) =>
                {
                    mDirInput.x = x;
                    mDirInput.y = y;
                    this.SendEvent(mDirInput);
                });

            transform.Find("RightStickArea")
                .GetComponent<StickDrag>()
                .OnDirChange += ((x, y) =>
                {
                    Debug.Log($"��ҷ Pos:[{x},{y}]");
                });
        }
    }
}