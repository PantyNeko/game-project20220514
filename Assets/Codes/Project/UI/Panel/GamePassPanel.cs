using UnityEngine.SceneManagement;

namespace PlatformShoot
{
    public class GamePassPanel : PlatformShootUIController
    {
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "ResetGameBtn":
                    SceneManager.LoadScene("SampleScene");
                    break;
            }
        }
    }
}