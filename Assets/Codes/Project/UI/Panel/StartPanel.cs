using UnityEngine;
using UnityEngine.UI;
using QFramework;

namespace PlatformShoot
{
    public class StartPanel : PlatformShootUIController
    {
        private void Start()
        {
            this.GetSystem<ITimerSystem>();
        }
        // 这就是按钮的绑定使用 Toggle的用法类似
        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "StartBtn":
                    this.SendCommand(new NextLevelCommand("SampleScene"));
                    break;
                case "ExitBtn":
                    Application.Quit();
                    break;
            }
        }
        protected override void OnValueChanged(string name, bool value)
        {
            // 当 Toggle 发生变更 并返回 true 时
            if (value)
            {
                switch (name)
                {
                    case "xxx":
                        // DoTo
                        break;
                    case "yyy":
                        // DOTO
                        break;
                }
            }
        }
    }
}