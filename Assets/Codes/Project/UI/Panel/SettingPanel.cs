using UnityEngine.UI;
using QFramework;

namespace PlatformShoot
{
    public class SettingPanel : PlatformShootUIController
    {
        private IGameAudioModel gameAudio;

        private void Start()
        {
            gameAudio = this.GetModel<IGameAudioModel>();

            GetControl<Slider>("BgmVolume").value = gameAudio.BgmVolume.Value;
            GetControl<Slider>("SoundVolume").value = gameAudio.SoundVolume.Value;
        }

        protected override void OnClick(string name)
        {
            switch (name)
            {
                case "CloseBtn":
                    this.GetSystem<IUGUISystem>().HidePanel<SettingPanel>();
                    break;
            }
        }

        protected override void OnValueChanged(string name, float value)
        {
            switch (name)
            {
                case "BgmVolume":
                    gameAudio.BgmVolume.Value = value;
                    break;
                case "SoundVolume":
                    gameAudio.SoundVolume.Value = value;
                    break;
            }
        }
    }
}