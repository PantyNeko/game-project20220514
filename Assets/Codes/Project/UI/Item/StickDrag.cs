using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace PlatformShoot
{
    public class StickDrag : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        private RectTransform mStickBgTrans;

        private Transform mStickItemTrans;

        public event Action<int, int> OnDirChange;

        private float maxRange;

        private void Start()
        {
            mStickBgTrans = transform.Find("StickBg") as RectTransform;
            maxRange = mStickBgTrans.rect.width * 0.5f;
            //mStickBgTrans.gameObject.SetActive(false);
            mStickItemTrans = mStickBgTrans.Find("StickItem");
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData data)
        {
            //RectTransformUtility.ScreenPointToLocalPointInRectangle(
            //    transform as RectTransform, data.position, data.pressEventCamera, out Vector2 localPos);
            //mStickBgTrans.localPosition = localPos;
            //mStickBgTrans.gameObject.SetActive(true);
        }

        void IDragHandler.OnDrag(PointerEventData data)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                // 需要改变位置对象的父级,当前屏幕鼠标位置,UI摄像机,返回一个相对坐标
                mStickBgTrans, data.position, data.pressEventCamera, out Vector2 localPos);
            var normal = localPos.normalized;
            // 发送当前摇杆的向量信息
            OnDirChange?.Invoke(normal.x > 0 ? 1 : -1, normal.y > 0 ? 1 : -1);
            // 根据范围更新坐标
            mStickItemTrans.localPosition = localPos.magnitude > maxRange ? normal * maxRange : localPos;
            //Debug.Log("拖曳ing" + dirInput.InputX);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            //mStickBgTrans.gameObject.SetActive(false);
            mStickItemTrans.localPosition = Vector2.zero;
            OnDirChange?.Invoke(0, 0);
        }
    }
}